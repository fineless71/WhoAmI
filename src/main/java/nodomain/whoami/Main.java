package nodomain.whoami;

import net.fabricmc.api.ModInitializer;
import nodomain.whoami.config.Config;

public class Main implements ModInitializer {
	@Override
	public void onInitialize() {
		Config.load();
	}
}
