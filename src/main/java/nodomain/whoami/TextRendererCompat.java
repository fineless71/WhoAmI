package nodomain.whoami;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;

public class TextRendererCompat {
	/* TODO: For performance, see if we can avoid reflection, eg. via a hack like draw centered
	See if method_956 is present in both 1.7.10 & 1.8.9
	*/

	private static GameVersion gameVersion;
	private static final MethodHandle drawMethodHandle;
	private static final MethodHandle drawLayerMethodHandle;

	static {
		Method drawMethod = null;
		MethodHandle drawMethodHandleTemp = null;

		Method drawLayerMethod = null;
		MethodHandle drawLayerMethodHandleTemp = null;

		if (FabricLoader.getInstance().getMappingResolver().getCurrentRuntimeNamespace().equals("intermediary")) {
			if (drawMethod == null) drawMethod = getMethod(GameVersion.V1_7, TextRenderer.class, "method_4246", String.class, int.class, int.class, int.class, boolean.class); /* 1.7.10 & 1.12.2 */
			if (drawMethod == null) drawMethod = getMethod(GameVersion.V1_8, TextRenderer.class, "method_9417", String.class, float.class, float.class, int.class, boolean.class); /* 1.8.9 */

			if (drawLayerMethod == null) drawLayerMethod = getMethod(GameVersion.V1_7, TextRenderer.class, "method_957", String.class, int.class, int.class, int.class, boolean.class); /* 1.7.10 & 1.12.2 */
			if (drawLayerMethod == null) drawLayerMethod = getMethod(GameVersion.V1_8, TextRenderer.class, "method_957", String.class, float.class, float.class, int.class, boolean.class); /* 1.8.9 */
		} else if (FabricLoader.getInstance().getMappingResolver().getCurrentRuntimeNamespace().equals("named")) {
			if (drawMethod == null) drawMethod = getMethod(GameVersion.V1_7, TextRenderer.class, "draw", String.class, int.class, int.class, int.class, boolean.class); /* 1.7.10 & 1.12.2 */
			if (drawMethod == null) drawMethod = getMethod(GameVersion.V1_8, TextRenderer.class, "draw", String.class, float.class, float.class, int.class, boolean.class); /* 1.8.9 */

			if (drawLayerMethod == null) drawLayerMethod = getMethod(GameVersion.V1_7, TextRenderer.class, "method_957", String.class, int.class, int.class, int.class, boolean.class); /* 1.7.10 & 1.12.2 */
			if (drawLayerMethod == null) drawLayerMethod = getMethod(GameVersion.V1_8, TextRenderer.class, "drawLayer", String.class, float.class, float.class, int.class, boolean.class); /* 1.8.9 */
		}

		drawMethod.setAccessible(true);
		drawLayerMethod.setAccessible(true);

		try {
			drawMethodHandleTemp = MethodHandles.lookup().unreflect(drawMethod);
			drawLayerMethodHandleTemp = MethodHandles.lookup().unreflect(drawLayerMethod);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		drawMethodHandle = drawMethodHandleTemp;
		drawLayerMethodHandle = drawLayerMethodHandleTemp;
	}

	private static Method getMethod(GameVersion onSuccess, Class<?> target, String name, Class<?>... args) {
		try {
			Method res = target.getDeclaredMethod(name, args);
			gameVersion = onSuccess;
			return res;
		} catch (NoSuchMethodException | SecurityException e) {}
		return null;
	}

	public static int draw(String text, float x, float y, int color, boolean shadow) {
		if (!shadow)
			return MinecraftClient.getInstance().textRenderer.draw(text, Math.round(x), Math.round(y), color);

		if (gameVersion.equals(GameVersion.V1_7))
			return draw$1_7(text, Math.round(x), Math.round(y), color, shadow);
		else if (gameVersion.equals(GameVersion.V1_8))
			return draw$1_8(text, x, y, color, shadow);
		return 0;
	}

	public static int draw(String text, int x, int y, int color, boolean shadow) {
		return draw(text, (float)x, (float)y, color, shadow);
	}

	public static int draw(String text, float x, float y, int color) {
		return draw(text, x, y, color, false);
	}

	public static int draw(String text, int x, int y, int color) {
		return draw(text, (float)x, (float)y, color, false);
	}

	public static int drawLayer(String text, float x, float y, int color, boolean shadow) {
		if (gameVersion.equals(GameVersion.V1_7))
			return drawLayer$1_7(text, Math.round(x), Math.round(y), color, shadow);
		else if (gameVersion.equals(GameVersion.V1_8))
			return drawLayer$1_8(text, x, y, color, shadow);
		return 0;
	}

	private static int draw$1_7(String text, int x, int y, int color, boolean shadow) {
		try {
			return (int) drawMethodHandle.invokeExact(MinecraftClient.getInstance().textRenderer, text, x, y, color, shadow);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return 0;
	}

	private static int draw$1_8(String text, float x, float y, int color, boolean shadow) {
		try {
			return (int) drawMethodHandle.invokeExact(MinecraftClient.getInstance().textRenderer, text, x, y, color, shadow);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return 0;
	}

	private static int drawLayer$1_7(String text, int x, int y, int color, boolean shadow) {
		try {
			return (int) drawLayerMethodHandle.invokeExact(MinecraftClient.getInstance().textRenderer, text, x, y, color, shadow);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return 0;
	}

	private static int drawLayer$1_8(String text, float x, float y, int color, boolean shadow) {
		try {
			return (int) drawLayerMethodHandle.invokeExact(MinecraftClient.getInstance().textRenderer, text, x, y, color, shadow);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int getStringWidth(String text) {
		return MinecraftClient.getInstance().textRenderer.getStringWidth(text);
	}

	public static int getFontHeight() {
		return MinecraftClient.getInstance().textRenderer.fontHeight;
	}
}
