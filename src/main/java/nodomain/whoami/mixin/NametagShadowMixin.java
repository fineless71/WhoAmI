package nodomain.whoami.mixin;

import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.entity.EntityRenderer;
import nodomain.whoami.TextRendererCompat;
import nodomain.whoami.config.Config;

@Mixin(EntityRenderer.class)
public class NametagShadowMixin {
	@Unique
	private static double nametagFixOffset = 0.1d; /* Can be smaller in 1.8.9 */

	@Redirect(method = "renderLabelIfPresent(Lnet/minecraft/entity/Entity;Ljava/lang/String;DDDI)V",
	at = @At(
		value = "INVOKE",
		target = "Lnet/minecraft/client/font/TextRenderer;draw(Ljava/lang/String;III)I"
	))
	private int renderLabelWithShadow(TextRenderer textRenderer, String text, int x, int y, int color) {
		if (!Config.enabled || !Config.nametagShadow)
			return textRenderer.draw(text, x, y, color);

		if (!Config.nametagShadowFix)
			return TextRendererCompat.draw(text, x, y, color, true);

		/* Nametag shadow fix */
		int ret;
		GL11.glTranslated(0, 0, nametagFixOffset);
		ret = TextRendererCompat.drawLayer(text, x + 1, y + 1, color, true);
		GL11.glTranslated(0, 0, -nametagFixOffset);
		ret = Math.max(ret, TextRendererCompat.drawLayer(text, x, y, color, false));
		return ret;
	}
}
