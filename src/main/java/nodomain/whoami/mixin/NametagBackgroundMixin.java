package nodomain.whoami.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.entity.EntityRenderer;
import nodomain.whoami.config.Config;

@Mixin(EntityRenderer.class)
public class NametagBackgroundMixin {
	@Redirect(method = "renderLabelIfPresent(Lnet/minecraft/entity/Entity;Ljava/lang/String;DDDI)V",
	at = @At(
		value = "INVOKE",
		target = "Lnet/minecraft/client/render/Tessellator;vertex(DDD)V"
	),
	require = 0)
	private void removeNametagBackground(Tessellator tessellator, double x, double y, double z) {
		if (!Config.enabled || Config.nametagBackground) {
			/* Minecraft 1.8.9 uses BufferBuilder.vertex */
			tessellator.vertex(x, y, z);
			return;
		}
	}
}
