package nodomain.whoami.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.CancellationException;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import nodomain.whoami.config.Config;

@Mixin(LivingEntityRenderer.class)
public class ShowNametagMixin {
	@Inject(method = "method_5781(Lnet/minecraft/entity/LivingEntity;)Z",
	at = @At("HEAD"),
	cancellable = true,
	require = 0)
	private void shouldShowNametag$1_7_10(LivingEntity livingEntity, CallbackInfoReturnable<Boolean> info) {
		if (!Config.enabled) return;
		if (!Config.showOwnNametag) return;

		Config.setPlayerField();

		try {
			info.setReturnValue(
				MinecraftClient.isHudEnabled()
				/*&& livingEntity != this.dispatcher.field_6482*/
				&& !livingEntity.isInvisibleTo((PlayerEntity)Config.playerField.get(MinecraftClient.getInstance()))
				&& livingEntity.rider == null
			);
		} catch (CancellationException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Inject(method = "hasLabel(Lnet/minecraft/entity/LivingEntity;)Z",
	at = @At("HEAD"),
	cancellable = true,
	require = 0)
	private void shouldShowNametag$1_8_9(LivingEntity livingEntity, CallbackInfoReturnable<Boolean> info) {
		shouldShowNametag$1_7_10(livingEntity, info);
	}
}
