package nodomain.whoami.config;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;

public class ConfigScreen extends Screen {
	private Screen parent = null;

	public ConfigScreen(Screen parent) {
		this.parent = parent;
		Config.load();
	}

	@Override
	public void init() {
		this.buttons.add(new ButtonWidget(1, this.width / 2 - 100, 10, "Show Own Nametag: " + (Config.showOwnNametag ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(2, this.width / 2 - 100, 40, "Nametag Shadow: " + (Config.nametagShadow ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(3, this.width / 2 - 100, 70, "Nametag Background: " + (Config.nametagBackground ? "ON" : "OFF (1.7.x)")));
		this.buttons.add(new ButtonWidget(0, this.width-200-10, this.height-20-10, I18n.translate("gui.done")));
    }

	@Override
	protected void buttonClicked(ButtonWidget button) {
		if (button.id == 1) {
			Config.showOwnNametag = !Config.showOwnNametag;
			button.message = "Show Own Nametag: " + (Config.showOwnNametag ? "ON" : "OFF");
		}

		if (button.id == 2) {
			Config.nametagShadow = !Config.nametagShadow;
			button.message = "Nametag Shadow: " + (Config.nametagShadow ? "ON" : "OFF");
		}

		if (button.id == 3) {
			Config.nametagBackground = !Config.nametagBackground;
			button.message = "Nametag Background: " + (Config.nametagBackground ? "ON" : "OFF (1.7.x)");
		}

		if (button.id == 0) {
			this.client.setScreen(this.parent);
		}
    }

	@Override
    public void render(int mouseX, int mouseY, float tickDelta) {
        this.renderBackground();
        super.render(mouseX, mouseY, tickDelta);
    }

	@Override
	public void removed() {
		Config.save();
    }

	public static boolean isEnabled() {
		return Config.enabled;
	}

	public static void setEnabled(boolean enabled) {
		Config.enabled = enabled;
	}
}
