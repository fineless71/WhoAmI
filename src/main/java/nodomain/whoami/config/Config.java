package nodomain.whoami.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import net.minecraft.client.MinecraftClient;

public class Config {
	public static transient final Gson gson = new GsonBuilder()
		.excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT)
		.setPrettyPrinting()
		.create();
	public static transient final File file = new File("config/WhoAmI.json");
	public static transient Field playerField = null;
	public static boolean enabled = true;
	public static boolean showOwnNametag = true;
	public static boolean nametagShadow = false;
	public static boolean nametagShadowFix = true;
	public static boolean nametagBackground = true;

	public static void save() {
		Config.file.getParentFile().mkdirs();
		try {
			Config.file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		try (Writer configWriter = new FileWriter(Config.file)) {
			Config.gson.toJson(new Config(), configWriter);
		} catch (JsonIOException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void load() {
		try (JsonReader configReader = new JsonReader(new FileReader(Config.file))) {
			gson.fromJson(configReader, Config.class);
		} catch (JsonIOException | JsonSyntaxException e) {
			e.printStackTrace();
			Config.save(); /* Regenerate from whatever config we have in memory */
		} catch (FileNotFoundException e) {
			/* Config hasn't been saved yet */
			Config.save();
		} catch (IOException e) {}
	}

	public static void setPlayerField() {
		if (playerField != null) return;

		/* 1.7.10 */
		try {
			playerField = MinecraftClient.class.getDeclaredField("field_6279");
		} catch (NoSuchFieldException | SecurityException e) {}

		if (playerField != null) return;

		/* 1.8.9 Obf */
		try {
			playerField = MinecraftClient.class.getDeclaredField("field_10310");
		} catch (NoSuchFieldException | SecurityException e) {}

		if (playerField != null) return;

		/* 1.8.9 Deobf */
		try {
			playerField = MinecraftClient.class.getDeclaredField("player");
		} catch (NoSuchFieldException | SecurityException e) {}
	}
}
